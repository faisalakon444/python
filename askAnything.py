import wikipedia
import wolframalpha
from gtts import gTTS
import speech_recognition as sr
import os
import re
import requests
import win32com.client as wincl

speak=wincl.Dispatch('SAPI.SpVoice')

def myCommand():

    r = sr.Recognizer()
   
    
    with sr.Microphone() as source:
        
        speak.speak('tell me')
        r.pause_threshold = 1
        r.adjust_for_ambient_noise(source, duration=1)
        audio = r.listen(source)

    try:
        command = r.recognize_google(audio).lower()
        print('You said: ' + command + '\n')
        talkToMe('You said: ' + command)

   
    except sr.UnknownValueError:
        
        speak.speak('Your last command couldn\'t be heard')
        command = myCommand();

    return command



def talkToMe(audio):
    
    speak.speak(audio)
def loop_question():
    while True:
        question = myCommand()
        if 'go back' in question:
            talkToMe('ok going back')
            break
        try:
            #wolframalpha
            app_id = "4LJUJ8-EREPTLT745"
            client = wolframalpha.Client(app_id)
            res = client.query(question)
            answer = next(res.results).text
            talkToMe(answer)
        except:
            #wikipedia
            print (wikipedia.summary(question))
        
            #print("sorry, i don't know")
        
        





