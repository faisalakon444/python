from gtts import gTTS
import speech_recognition as sr
import os
import re
import webbrowser
import smtplib
import requests

import win32com.client as wincl
import findmp3
import openApplications
import askAnything
speak=wincl.Dispatch('SAPI.SpVoice')

def talkToMe(audio):
    
    speak.speak(audio)
def myCommand():
    

    r = sr.Recognizer()
   
    
    with sr.Microphone() as source:
        
        speak.speak('tell me')
        r.pause_threshold = 1
        r.adjust_for_ambient_noise(source, duration=1)
        audio = r.listen(source)

    try:
        command = r.recognize_google(audio).lower()
        print('You said: ' + command + '\n')
        talkToMe('You said: ' + command)

    
    except sr.UnknownValueError:
        
        speak.speak('Your last command couldn\'t be heard')
        command = myCommand();

    return command


def assistant(command):
    

    if 'open reddit' in command:
        reg_ex = re.search('open reddit (.*)', command)
        url = 'https://www.reddit.com/'
        if reg_ex:
            subreddit = reg_ex.group(1)
            url = url + 'r/' + subreddit
        webbrowser.open(url)
        print('Done!')

    elif 'open website' in command:
        reg_ex = re.search('open website (.+)', command)
        if reg_ex:
            domain = reg_ex.group(1)
            url = 'https://www.' + domain
            webbrowser.open(url)
            print('Done!')
        else:
            pass

    elif 'what are you doing' in command:
        talkToMe('Just doing my thing')
    elif 'can i ask you something' in command:
        talkToMe('yes you can')
        askAnything.loop_question()
    elif 'joke' in command:
        res = requests.get(
                'https://icanhazdadjoke.com/',
                headers={"Accept":"application/json"}
                )
        if res.status_code == requests.codes.ok:
            talkToMe(str(res.json()['joke']))
        else:
            talkToMe('oops!I ran out of jokes')

    elif 'find' in command:
        reg_ex = re.search('find (.*)', command)
        

        if reg_ex:
            findmp3.search_all_drives(reg_ex.group(1))
        print('done')

    elif 'open' in command:
        reg_ex = re.search('open (.*)', command)
        

        if reg_ex:
            openApplications.openApplication(reg_ex.group(1)+'.exe')
        print('done')

    elif 'shutdown' in command:
        talkToMe('ok, i am going to shutdown this pc')
        os.system("shutdown /s /t 1")
        
        
        
            

        
    
    


talkToMe('I am ready for your command')

while True:
    assistant(myCommand())
